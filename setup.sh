if [ ! $(which conda) ]
then
    echo Conda is not installed. Please install conda.
    exit 1
fi

if ! conda env list | grep CC
then
    conda env create -f cc_environment.yml
fi


conda activate CC

export CCDIR=$(dirname $BASH_SOURCE)
export PYTHONPATH=$PYTHONPATH:$CCDIR

export PATH=$PATH:$CCDIR/bin

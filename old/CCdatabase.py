import os

import numpy as np
import scipy as scp
import scipy.interpolate


DATABASE_DIR = os.path.join(os.environ["CCDIR"], "database")
SPECS_DIR    = os.path.join(DATABASE_DIR, "efficiency_digitization")

power_ratio_files = []


paper_signals = dict(
 Sp  = (400,),
 Qz  = (346,),
 BaF = (570, 960),
 CaF = (580, 630, 770),
 LiF = (331, 680, 530),
)

seattle_irradiation_mapping = dict(
 Sp_B1_007 = "lowγ  w/  Cd",
 Sp_B1_008 = "back  w/o Cd",
 Sp_B1_009 = "highγ w/o Cd",
 Sp_B2_022 = "lowγ  w/  Cd",
 Sp_B2_031 = "far"         ,
 Sp_B2_033 = "highγ w/  Cd",
 Sp_B2_034 = "lowγ  w/o Cd",
 Sp_B2_035 = "back  w/  Cd",

 Qz_B1_042 = "lowγ  w/  Cd",
 Qz_B1_046 = "lowγ  w/  Cd",
 Qz_B1_048 = "highγ w/o Cd",
 Qz_B1_049 = "back  w/  Cd",
 Qz_B1_050 = "far"         ,
 Qz_B1_052 = "lowγ  w/o Cd",
 Qz_B1_053 = "back  w/  Cd",
 Qz_B2_075 = "highγ w/  Cd",

LiF_B1_131 = "highγ w/  Cd",
LiF_B2_152 = "lowγ  w/  Cd",
LiF_B2_156 = "lowγ  w/  Cd",
LiF_B2_157 = "back  w/  Cd",
LiF_B2_158 = "far"         ,
LiF_B2_159 = "lowγ  w/o Cd",
LiF_B2_160 = "highγ w/  Cd",

BaF_B1_162 = "lowγ  w/  Cd",
BaF_B1_164 = "highγ w/o Cd",
BaF_B1_165 = "lowγ  w/  Cd",
BaF_B1_166 = "back  w/o Cd",
BaF_B1_167 = "far"         ,
BaF_B1_168 = "lowγ  w/o Cd",
BaF_B1_169 = "back  w/  Cd",
BaF_B2_196 = "highγ w/  Cd",

CaF_B2_302 = "lowγ  w/  Cd",
CaF_B1_283 = "highγ w/  Cd",
CaF_B2_311 = "lowγ  w/o Cd",
CaF_B2_312 = "back  w/  Cd",
CaF_B2_317 = "lowγ  w/  Cd",
CaF_B2_318 = "highγ w/o Cd",
CaF_B2_319 = "back  w/o Cd",
CaF_B2_320 = "far"         ,
)

californication_irradiation_mapping = dict(
Sp_B2_036 = "HD 2w",
Sp_B2_037 = "HD 2w",
Sp_B2_038 = "HD 1w",
Sp_B2_039 = "LD 2w",
Sp_B1_004 = "LD 1w",

Qz_B2_073 = "HD 2w",
Qz_B2_074 = "HD 2w",
Qz_B2_076 = "HD 1w",
Qz_B2_077 = "LD 2w",
Qz_B2_078 = "LD 1w",
Qz_B2_079 = "HD 1w",

CaF_B1_281 = "HD 2w",
CaF_B1_282 = "HD 2w",
CaF_B1_289 = "HD 1w",
CaF_B1_290 = "LD 2w",
CaF_B1_292 = "LD 1w",

BaF_B1_170 = "HD 2w",
BaF_B1_171 = "HD 2w",
BaF_B1_172 = "HD 1w",
BaF_B1_173 = "LD 2w",
BaF_B1_174 = "LD 1w"
)


position_yield_caf = {
 4 : 3.59657251e09,
 5 : 5.26197908e09,
 6 : 4.50945873e09,
 7 : 3.54644733e09,
 8 : 3.92451181e09,
 9 : 4.68942638e09,
10 : 3.90299400e09,
11 : 3.70297055e09,
}

position_yield_zno = {
4  : 3.68933231e12,
5  : 3.11775851e12,
6  : 2.88590920e12,
7  : 1.79120779e12,
8  : 1.84293263e12,
9  : 4.04556997e12,
10 : 2.36505869e12,
11 : 3.20975664e12,
}

position_yield_zno_2 = {
4  : 3.55190831e12,
5  : 2.95825386e12,
6  : 2.75676249e12,
7  : 1.76921162e12,
8  : 1.79842116e12,
9  : 3.42596626e12,
10 : 2.29504277e12,
11 : 3.15279959e12,
}

position_yield_zno_3 = {
4  : 3.72925066e12,
5  : 3.18109012e12,
6  : 2.95371357e12,
7  : 1.78385100e12,
8  : 1.81426937e12,
9  : 3.74104972e12,
10 : 2.36615464e12,
11 : 3.26411933e12,
}

position_yield_zno_4 = {
4  : 3.69917000e12,
5  : 3.11308441e12,
6  : 2.94102058e12,
7  : 1.75374891e12,
8  : 1.78052063e12,
9  : 4.08125206e12,
10 : 2.35691198e12,
11 : 3.24186167e12,
}

position_factor_caf = {
4  : 0.6835018640933099,
5  : 1.0000000000000000,
6  : 0.8569891026628711,
7  : 0.6739759463277836,
8  : 0.7458242897461310,
9  : 0.8911906164400790,
10 : 0.7417349899460262,
11 : 0.7037220204987968,
}

position_factor_zno = {
4  : 0.9119437655372550,
5  : 0.7706598904522602,
6  : 0.7133504620989393,
7  : 0.4427578331526952,
8  : 0.4555433836399713,
9  : 1.0000000000000000,
10 : 0.5846045709486433,
11 : 0.7934003529146842,
}

position_factor_zno_2 = {
4  : 1.0000000000000000,
5  : 0.8328632412623671,
6  : 0.7761356026609140,
7  : 0.4981017157609560,
8  : 0.5063253357154951,
9  : 0.9645424261553119,
10 : 0.6461435865175148,
11 : 0.8876354125611280,
}

position_factor_zno_3 = {
4  : 0.99684605529974660,
5  : 0.85032019212439680,
6  : 0.78954138393832280,
7  : 0.47683167347907335,
8  : 0.48496264647134630,
9  : 1.00000000000000000,
10 : 0.63248414689383100,
11 : 0.87251428554681740,
}

position_factor_zno_4 = {
4  : 0.90638116616748950,
5  : 0.76277680509698550,
6  : 0.72061723536885380,
7  : 0.42970855163493554,
8  : 0.43626823395805614,
9  : 1.00000000000000000,
10 : 0.57749728437281280,
11 : 0.79433017677242460,
}

def load_spectral_radiance():
    filename = os.path.join(SPECS_DIR, "RadianceDigi.csv")
    wls, radiance = np.loadtxt(filename, delimiter = ",").T
    return scp.interpolate.interp1d(wls, radiance)


def load_grating2_efficiencies():
    filename = os.path.join(SPECS_DIR, "1200@600PwaveMono.csv")
    wls, eff = np.loadtxt(filename, delimiter = ",", skiprows=1).T
    effP     = scp.interpolate.interp1d(wls * 1e3, eff, bounds_error=False, fill_value="extrapolate")

    filename = os.path.join(SPECS_DIR, "1200@600SwaveMono.csv")
    wls, eff = np.loadtxt(filename, delimiter = ",", skiprows=1).T
    effS     = scp.interpolate.interp1d(wls * 1e3, eff, bounds_error=False, fill_value="extrapolate")

    def summed_eff(x):
        return effP(x) + effS(x)

    return effP, effS, summed_eff


def load_power_ratio(filename=os.path.join(DATABASE_DIR, "power_ratio.npy")):
    data = np.load(filename)
    wls  = data[0 , 1:]
    pols = data[1:, 0 ]
    data = data[1:, 1:]

    x, y = np.meshgrid(wls, pols)
    return scp.interpolate.interp2d(x, y, data, kind="cubic")


def load_LiF_absorption():
    filename = os.path.join(DATABASE_DIR, "LiF_absorption.dat")
    Es, mus  = np.loadtxt(filename, skiprows=3).T
    return scp.interpolate.interp1d(Es, mus, kind="cubic", bounds_error=False, fill_value="extrapolate")


def position_correction(pos):
    return 1 / position_yield_zno[pos]


def spf_position(ex_wl):
    if 390 <= ex_wl < 440: return 1
    if 440 <= ex_wl < 490: return 2
    if 490 <= ex_wl < 540: return 3
    if 540 <= ex_wl < 590: return 4
    if 590 <= ex_wl < 640: return 5
    return 6


def lpf_position(ex_wl):
    if 350 <= ex_wl < 400: return 1
    if 400 <= ex_wl < 450: return 2
    if 450 <= ex_wl < 500: return 3
    if 500 <= ex_wl < 550: return 4
    if 550 <= ex_wl < 600: return 5
    return 6


def flipper_position(ex_wl):
    return "up" if ex_wl >= 360 else "down"

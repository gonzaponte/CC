"""
Auxiliary functions that have nothing to do with the CC
data manipulation.
"""

import os
import sys
import time
import shutil
import functools
import contextlib

import numpy as np


def lmap(*args):
    return list(map(*args))


def tmap(*args):
    return tuple(map(*args))


def npmap(*args):
    return np.array(lmap(*args))


def map_keys(dic, f):
    return {f(k) : v for k, v in dic.items()}


def map_values(dic, f):
    return {k : f(v) for k, v in dic.items()}


def map_keys_values(dic, fk, fv):
    return {fk(k) : fv(v) for k, v in dic.items()}


def timeme(f):
    @functools.wraps(f)
    def proxy(*args, **kwargs):
        t0  = time.time()
        out = f(*args, **kwargs)
        t1  = time.time()
        dt  = t1 - t0
        print(f"Function {f.__name__} took {dt:.3f} s ({dt / 60 : .3f} min, {dt / 3600 : .2f} h)")
        return out
    return proxy


@contextlib.contextmanager
def temp_var(value):
    yield value


@contextlib.contextmanager
def silent():
    previous_stdout = sys.stdout
    sys.stdout = open(os.devnull, "w")
    try:
        yield
    finally:
        sys.stdout.close()
        sys.stdout = previous_stdout


def join_many(main, sub):
    return tuple(map(functools.partial(os.path.join, main), sub))


def alphabetic_order(folder):
    return sorted(filter(os.path.isdir, join_many(folder, os.listdir(folder))))


def copy_file(source, destination, verbose=True):
    if verbose:
        print(f"{source} -> {destination}")
    shutil.copy(source, destination)


def tee(filename):
    """
    Usage: print = tee(filename)
    """
    screen = sys.stdout
    file   = open(filename, "w")
    def print_to_many(*args):
        message = " ".join(map(str, args)) + "\n"
        for output in (screen, file):
            output.write(message)
            output.flush()
    return print_to_many

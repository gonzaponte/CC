import re

import numpy  as np
import pandas as pd


def fstring_to_re(fs):
    return fs.replace("{", "(?P<").replace("}", ">.*)")


BASELINE = 3100

EMPTY_ARRAY = np.array([], dtype=float)
EMPTY_DF    = pd.DataFrame(dict(state      = np.array([], dtype=int  ),
                                crystal    = np.array([], dtype=int  ),
                                grating    = np.array([], dtype=int  ),
                                looparound = np.array([], dtype=bool ),
                                full_scan  = np.array([], dtype=bool ),
                                slit_width = np.array([], dtype=int  ),
                                exposure   = np.array([], dtype=float),
                                exp_index  = np.array([], dtype=int  ),
                                baseline   = np.array([], dtype=float),
                                ex_wl      = np.array([], dtype=float),
                                em_wl      = np.array([], dtype=float),
                                counts     = np.array([], dtype=int  ),
                                rate       = np.array([], dtype=float),
                                power      = np.array([], dtype=float),
                                dquanta    = np.array([], dtype=float)))

SIGNAL_TEMPLATE_1     = "state_{state}_crystal_{crystal}_ex_wl_{ex_wl}_exposure_{exposure}_signal.asc"
SIGNAL_TEMPLATE_2     = "state_{state}_crystal_{crystal}_ex_wl_{ex_wl}_exposure_{exposure}_grating_{grating}_looparound_{looparound}_signal.asc"
SIGNAL_TEMPLATE_3     = "state_{state}_crystal_{crystal}_fullscan_{full_scan}_exwl_{ex_wl}_monograting_{mono_grating}_exposure_{exposure}_signal.asc"
BACKGROUND_TEMPLATE_1 = "background_{when}_crystal_{crystal}_exposure_{exposure}.asc"
BACKGROUND_TEMPLATE_2 = "background_crystal_{crystal}_exposure_{exposure}.asc"
BACKGROUND_TEMPLATE_3 = "background_crystal_{crystal}_fullscan_{full_scan}_exposure_{exposure}_signal.asc"
AMBIENT_TEMPLATE_1    = "ambient_{when}_crystal_{crystal}_exposure_{exposure}.asc"
AMBIENT_TEMPLATE_2    = "ambient_crystal_{crystal}_exposure_{exposure}.asc"
AMBIENT_TEMPLATE_3    = "ambient_crystal_{crystal}_fullscan_{full_scan}_exposure_{exposure}_signal.asc"
BASELINE_TEMPLATE_3   = "baseline_crystal_{crystal}_fullscan_{full_scan}_{state}_signal.asc"

SIGNAL_PATTERN_1     = re.compile(fstring_to_re(    SIGNAL_TEMPLATE_1))
SIGNAL_PATTERN_2     = re.compile(fstring_to_re(    SIGNAL_TEMPLATE_2))
SIGNAL_PATTERN_3     = re.compile(fstring_to_re(    SIGNAL_TEMPLATE_3))
BACKGROUND_PATTERN_1 = re.compile(fstring_to_re(BACKGROUND_TEMPLATE_1))
BACKGROUND_PATTERN_2 = re.compile(fstring_to_re(BACKGROUND_TEMPLATE_2))
BACKGROUND_PATTERN_3 = re.compile(fstring_to_re(BACKGROUND_TEMPLATE_3))
AMBIENT_PATTERN_1    = re.compile(fstring_to_re(   AMBIENT_TEMPLATE_1))
AMBIENT_PATTERN_2    = re.compile(fstring_to_re(   AMBIENT_TEMPLATE_2))
AMBIENT_PATTERN_3    = re.compile(fstring_to_re(   AMBIENT_TEMPLATE_3))
BASELINE_PATTERN_3   = re.compile(fstring_to_re(  BASELINE_TEMPLATE_3))

SIGNAL_TEMPLATES     =     SIGNAL_TEMPLATE_3,     SIGNAL_TEMPLATE_2,     SIGNAL_TEMPLATE_1
BACKGROUND_TEMPLATES = BACKGROUND_TEMPLATE_3, BACKGROUND_TEMPLATE_2, BACKGROUND_TEMPLATE_1
AMBIENT_TEMPLATES    =    AMBIENT_TEMPLATE_3,    AMBIENT_TEMPLATE_2,    AMBIENT_TEMPLATE_1
BASELINE_TEMPLATES   =   BASELINE_TEMPLATE_3,

SIGNAL_PATTERNS      =      SIGNAL_PATTERN_3,      SIGNAL_PATTERN_2,      SIGNAL_PATTERN_1
BACKGROUND_PATTERNS  =  BACKGROUND_PATTERN_3,  BACKGROUND_PATTERN_2,  BACKGROUND_PATTERN_1
AMBIENT_PATTERNS     =     AMBIENT_PATTERN_3,     AMBIENT_PATTERN_2,     AMBIENT_PATTERN_1
BASELINE_PATTERNS    =    BASELINE_PATTERN_3,

#"state_(?P<count>.*)_crystal_(?P<crystal_number>.*)_ex_wl_(?P<ex_wl>.*)_exposure_(?P<exposure>.*)_signal.asc"

TYPE_MAPPING = {
"bool"    : np.bool ,
"int8"    : np.int  ,
"int16"   : np.int  ,
"int32"   : np.int  ,
"uint8"   : np.int  ,
"uint16"  : np.int  ,
"uint32"  : np.int  ,
"float16" : np.float,
"float32" : np.float,
"float64" : np.float
}

def combine_power(*powers):
    new_time  = np.concatenate([pwr.time  for pwr in powers])
    new_power = np.concatenate([pwr.power for pwr in powers])
    new_mean  = new_power.mean() if len(new_power) else 0
    new_std   = new_power.std () if len(new_power) else 0
    return Pwr_data(new_time, new_power, new_mean, new_std)


def combine_spectra(*spectra):
    combined_power = combine_power(*[spectrum.power for spectrum in spectra])
    all_metas      = [spectrum.meta for spectrum in spectra]

    scales = [combined_power.mean / spectrum.power.mean if spectrum.power.mean else 1 for spectrum in spectra]

    all_em_wls = np.concatenate([spectrum.em_wl         for spectrum        in     spectra         ])
    all_raws   = np.concatenate([spectrum.raw   * scale for spectrum, scale in zip(spectra, scales)])
    all_blss   = np.concatenate([spectrum.bls   * scale for spectrum, scale in zip(spectra, scales)])
    all_rates  = np.concatenate([spectrum.rate  * scale for spectrum, scale in zip(spectra, scales)])
    order      = np.argsort(all_em_wls)
    all_em_wls = all_em_wls[order]

    return CCD_data(all_em_wls,
                    interp1d(all_em_wls, all_raws [order], kind="linear"),
                    interp1d(all_em_wls, all_blss [order], kind="linear"),
                    interp1d(all_em_wls, all_rates[order], kind="linear"),
                    combined_power,
                    all_metas)


def combine_measurements(counts):
    return counts.mean(axis=0)


def sensible_std(x, qmin = 5, qmax = 95):
    x0 = np.percentile(x, qmin)
    x1 = np.percentile(x, qmax)
    return np.std(x[(x >= x0) & (x < x1)])


def pull(x, y, q=None):
    std = np.std if q is None else functools.partial(sensible_std, qmin=q, qmax=100-q)
    return (x - y) / np.sqrt(std(x)**2 + std(y)**2)


def is_saturated(counts, nmin=10):
    indices = []
    for i in range(nmin):
        counts = np.diff(counts)
        if 0 not in counts: return False

        indices.append(np.where(counts == 0)[0])

    for index in set(itertools.chain(*indices)):
        if all(index in iteration for iteration in indices): return True

    return False

def add_shorter_exposures(df):
    extras      = []
    hundred_seconds = filter_df(df, exposure=100)
    for crystal_no in set(hundred_seconds.crystal.values):
        crystal = filter_df(hundred_seconds, crystal=crystal_no)
        for exposure in [0.1, 1, 10]:
            extra          = filter_df(crystal, exposure=exposure).copy()
            baseline       = compute_baseline_int(extra.counts)
            spectrum       = (extra.counts - baseline) * exposure / 100 + baseline
            extra.counts   = spectrum.astype(df.dtypes["counts"])
            extra.exposure = exposure
            extras.append(extra)
    return pd.concat([df, *extras], ignore_index=True)


def add_100s_exposure(df):
    extras      = []
    ten_seconds = filter_df(df, exposure=10)
    for crystal in set(ten_seconds.crystal.values):
        extra          = filter_df(ten_seconds, crystal=crystal).copy()
        baseline       = compute_baseline_int(extra.counts)
        spectrum       = (extra.counts - baseline) * 10 + baseline
        extra.counts   = spectrum.astype(df.dtypes["counts"])
        extra.exposure = 100
        extras.append(extra)
    return pd.concat([df, *extras], ignore_index=True)


def load_backgrounds(folder):
    filenames = select_background_filenames(folder)
    spectra   = collections.defaultdict(dict)

    for filename in filenames:
        meta     = metadata_from_filename(filename)
        spectrum = load_ccd_file(filename)
        spectra[meta.crystal][meta.exposure][meta.when] = spectrum

    if not combine: return spectra

    combined = dict()
    for crystal_no, crystal in spectra.items():
        combined.setdefault(crystal_no, dict())
        for exposure, backgrounds in crystal.items():
            combined[crystal_no][exposure] = backgrounds["before"]#combine_spectra(*tuple(backgrounds.values()))
    return combined

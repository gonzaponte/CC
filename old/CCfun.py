import os
import glob
import datetime
import itertools
import functools
import collections

import numpy  as np
import pandas as pd

from scipy.interpolate import interp1d

from CCtypes import CCD_data
from CCtypes import Pwr_data
from CCtypes import Meta_data
from CCtypes import Meta_data2
from CCtypes import Measurement
from CCtypes import Experiment

from CCconst import             BASELINE
from CCconst import     SIGNAL_TEMPLATES
from CCconst import BACKGROUND_TEMPLATES
from CCconst import    AMBIENT_TEMPLATES
from CCconst import   BASELINE_TEMPLATES
from CCconst import      SIGNAL_PATTERNS
from CCconst import  BACKGROUND_PATTERNS
from CCconst import     AMBIENT_PATTERNS
from CCconst import    BASELINE_PATTERNS


def in_range(x, min, max):
    return (x >= min) & (x < max)


def try_to_cast(value):
    try                  : return int  (value)
    except ValueError    :
        try              : return float(value)
        except ValueError: return (True  if value.lower() == "true"  else
                                   False if value.lower() == "false" else value)


def is_saturated(counts):
    return np.count_nonzero(counts == 65535) > 10


def compute_baseline_int(counts):
    counts = counts[counts < counts.max()] # Removes possible saturation
    h      = np.bincount(counts.astype(int))
    return np.argmax(h)


def compute_baseline_float(data):
    median = np.median(data)
    window = np.abs(0.2 * median)
    bins   = np.linspace(median - window, median + window, 201)
    h, _   = np.histogram(data, bins)
    i      = np.argmax(h)
    return bins[i:i+2].mean()


def compute_baseline_between(x, y, xmin, xmax):
    return y[in_range(x, xmin, xmax)].mean()


def grating_from_blaze(blaze):
    blaze = str(blaze).strip()
    if   blaze == "300nm": return 1
    elif blaze == "500nm": return 2
    else                 : raise  ValueError


@functools.lru_cache()
def metadata_from_filename(filename, pattern=None):
    if pattern is None:
        patterns = SIGNAL_PATTERNS + AMBIENT_PATTERNS + BACKGROUND_PATTERNS + BASELINE_PATTERNS
        for pattern in patterns:
            try                  : return metadata_from_filename(filename, pattern)
            except AttributeError: pass
        raise RuntimeError("None of the filenames patterns worked")

    basename = os.path.basename(filename)
    match    = pattern.match(basename)
    values   = {key: try_to_cast(value) for key, value in match.groupdict().items()}

    try             : return Meta_data (**values)
    except TypeError: return Meta_data2(**values)


def state_number_from_filename(filename):
    return metadata_from_filename(filename).state


def crystal_number_from_filename(filename):
    return metadata_from_filename(filename).crystal


def select_crystal_filenames(folder, crystal, **kwargs):
    kwds_with_defaults = collections.defaultdict(lambda: "*")
    kwds_with_defaults.update(crystal = crystal)
    kwds_with_defaults.update(kwargs)

    for template in SIGNAL_TEMPLATES:
        wildcard  = template.format_map(kwds_with_defaults)
        filenames = glob.glob(os.path.join(folder, wildcard))
        if len(filenames): break
    return sorted(filenames, key=state_number_from_filename)


def select_ambient_filenames(folder, **kwargs):
    kwds_with_defaults = collections.defaultdict(lambda: "*")
    kwds_with_defaults.update(kwargs)

    for AMBIENT_TEMPLATE in AMBIENT_TEMPLATES:
        wildcard  = AMBIENT_TEMPLATE.format_map(kwds_with_defaults)
        filenames = glob.glob(os.path.join(folder, wildcard))
        if len(filenames): break
    return sorted(filenames, key=crystal_number_from_filename)


def select_background_filenames(folder, **kwargs):
    kwds_with_defaults = collections.defaultdict(lambda: "*")
    kwds_with_defaults.update(kwargs)

    for BACKGROUND_TEMPLATE in BACKGROUND_TEMPLATES:
        wildcard  = BACKGROUND_TEMPLATE.format_map(kwds_with_defaults)
        filenames = glob.glob(os.path.join(folder, wildcard))
        if len(filenames): break
    return sorted(filenames, key=crystal_number_from_filename)


def select_baseline_filenames(folder, **kwargs):
    kwds_with_defaults = collections.defaultdict(lambda: "*")
    kwds_with_defaults.update(kwargs)

    for BASELINE_TEMPLATE in BASELINE_TEMPLATES:
        wildcard  = BASELINE_TEMPLATE.format_map(kwds_with_defaults)
        filenames = glob.glob(os.path.join(folder, wildcard))
        if len(filenames): break
    return sorted(filenames, key=crystal_number_from_filename)


def select_extension_filenames(folder, extension):
    filenames = glob.glob(os.path.join(folder, f"*.{extension}"))
    return filenames


def rebin(wf, n=2, op=np.sum):
    if wf.size % n:
        pad = n - wf.size % n
        wf  = np.append(wf, [wf[-1]] * pad)

    return op(wf.reshape(wf.size // n, n), axis=1)


def power_to_quanta_rate(power, wl):
    quanta_rate  = power / 1.602e-19   # eV/s
    quanta_rate *=    wl / 1239.841984 # photons/s
    return quanta_rate


def raman_differential_cross_section(wavelength):
    A    = 5.33e-27         # cm2 / sr / molecule
    nu_i = 88e3             # cm-1
    nu_p = 1e7 / wavelength # cm-1
    nu_s = nu_p - 3400      # cm-1
    return A * nu_s**4 / (nu_i**2 - nu_p**2)**2


def raman_wavelength(wavelength):
    return (wavelength**-1 - 3400e-7)**-1


def raman_shift(wavelength):
    return raman_wavelength(wavelength) - wavelength


def crystal_type(crystal_id):
    ctype = crystal_id.split("_")[0].lower()
    return dict(qz  = "Qz" , sp  = "Sp",
                baf = "BaF", caf = "CaF", lif = "LiF", mgf="MgF",
                mgo = "MgO", zno = "ZnO",
                lit = "LiT").get(ctype)


def df_mask(df, selections=dict(), skip_failures=False, **other_selections):
    selections = dict(**selections, **other_selections)

    mask = np.ones(len(df), dtype=bool)
    for column, value in selections.items():
        try:
            mask &= df[column] == value
        except:
            if not skip_failures:
                raise
    return mask


def filter_df(df, selections=dict(), skip_failures=False, **other_selections):
    selection = df_mask(df, selections, skip_failures, **other_selections)
    if np.count_nonzero(selection) == 0:
        raise RuntimeError("Empty DataFrame")
    return df.loc[selection]


def datetime_from_meta(metadata):
    s = " ".join(metadata["remarks"][0].split(" ")[-3:])
    for suffix in "st nd rd th".split():
        s = s.replace(suffix, "")

    return datetime.datetime.strptime(s, "%B %d %Y")


def datetime_from_spectrum(spectrum):
    time_str   = spectrum.meta["Date and Time"]
    ms_index   = time_str.index(".")
    time_wo_ms = time_str[:ms_index] + time_str[ms_index + 4:]
    return datetime.datetime.strptime(time_wo_ms, "%c")


def filter_measurement(measurement, selections, skip_failures=False):
    crystals   = filter_df(measurement.crystals  , selections, skip_failures)
    references = filter_df(measurement.references, selections, skip_failures)
    background = filter_df(measurement.background, selections, skip_failures)
    return Measurement(crystals, references, background, measurement.metadata)


def filter_experiment(experiment, selections, skip_failures=False):
    return Experiment(filter_measurement(experiment.pre , selections, skip_failures),
                      filter_measurement(experiment.post, selections, skip_failures))


def remove_ex_wl(fulldf, ex_wls, delta=20):
    def select(df, ex_wl):
        selection = False
        for i in range(1, 5):
            selection |= in_range(df.em_wl.values, i * ex_wl - delta, i * ex_wl + delta)

        return df.loc[~selection]

    if not isinstance(ex_wls, (int, float)):
        dfs = [select(df, ex_wl) for ex_wl, df in fulldf.groupby("ex_wl")]
        return pd.concat(dfs, ignore_index=True)

    return select(fulldf, ex_wls)


def add_crystal_meta(df, meta):
    df["crystal_id"  ] = df.crystal.map(meta["crystal_mapping"])
    df["crystal_type"] = df.crystal.map(meta["crystal_type_mapping"])
    return df


def add_missing_backgrounds(df, which="Before"):
    dfs = [df]
    if 0 not in df.crystal.values:
        next_crystal = df.crystal.loc[df.crystal > 0].min()
        bkg0 = filter_df(df, crystal=next_crystal).copy()
        bkg0.crystal = 0
        dfs.append(bkg0)

    if 3 not in df.crystal.values:
        other_crystal = 2
        if   which == "Before": other_crystal = df.crystal.loc[df.crystal > 3].min()
        elif which != "After" : raise ValueError(f"Invalid value of argument which: {which}")

        bkg3 = filter_df(df, crystal=other_crystal).copy()
        bkg3.crystal = 3
        dfs.append(bkg3)

    return pd.concat(dfs, ignore_index=True)


def add_missing_baselines(df, target_crystals=None):
    dfs = [df]

    if target_crystals is None:
        target_crystals = range(12)

    for i in target_crystals:
        if i not in df.crystal.values:
            next_crystal = df.crystal.loc[df.crystal > i].min()
            bsl = filter_df(df, crystal=next_crystal).copy()
            bsl.crystal = i
            dfs.append(bsl)

    return pd.concat(dfs, ignore_index=True)


def add_missing_exposures(df, target_exposures, baselines=None):
    extras           = []
    target_exposures = np.asarray(target_exposures)
    missing          = ~np.in1d(target_exposures, df.exposure.drop_duplicates().values)
    reference_expo   = df.exposure.max()
    for crystal_no in df.crystal.drop_duplicates():
        reference = filter_df(df, exposure=reference_expo, crystal=crystal_no)
        for exposure in target_exposures[missing]:
            extra          = reference.copy()
            if baselines is None:
                baseline   = extra.counts.min()
            else:
                baseline   = filter_df(baselines, crystal=crystal_no).counts.values
            spectrum       = (extra.counts.values - baseline) * exposure / reference_expo + baseline
            extra.counts   = spectrum.astype(df.dtypes["counts"])
            extra.exposure = exposure
            extras.append(extra)
    return pd.concat([df, *extras], ignore_index=True)


def process(data, baselines):
    if isinstance(baselines, (int, float)):
        data["bkgsub"] = data.counts.values - baselines
    else:
        data["bkgsub" ]  = np.nan
        for crystal_no, baseline in baselines.groupby("crystal"):
            if crystal_no not in data.crystal: continue

            selection = df_mask  (data, dict(crystal=crystal_no))
            baseline  = filter_df(baselines, crystal=crystal_no ); assert len(baseline) == 1600
            baseline  = np.tile  (baseline.counts.values,
                                  np.count_nonzero(selection) // len(baseline))
            data.loc[selection, "bkgsub"] = data.loc[selection, "counts"].values - baseline

    data["rate"   ]  = data.bkgsub / data.exposure
    data["quantar"]  = power_to_quanta_rate(data.power * 100, data.ex_wl) # *100 to account for beam splitter
    data["dquanta"]  = data.rate   / data.quantar


def longer_than_exwl(df, ex_wl=None, delta=30):
    if ex_wl is None:
        dfs = [longer_than_exwl(subdf, ex) for ex, subdf in df.groupby("ex_wl")]
        return pd.concat(dfs)
    return df.loc[df.em_wl.values > ex_wl + delta]


def add_timedeta(df):
    df["time_delta"] = df.timestamp - df.timestamp.min()


def find_crystal_position(crystal_id, metadatas):
    def search(metadata):
        for pos, id in metadata["crystal_mapping"].items():
            if id == crystal_id:
                return pos
    if not isinstance(metadatas, dict):
        for metadata in metadatas:
            s = search(metadata)
            if s is not None: return s
    else:
        return search(metadata)
    raise ValueError(f"{crystal_id} not in mapping")


def average_baselines(df):
    if not len(df):
        return df
    return df.groupby("crystal em_wl".split(), as_index=False).aggregate("mean")


_SPIKES_THRESHOLD = 150

def remove_spikes(data, threshold=_SPIKES_THRESHOLD):
    diff    = np.append([0], np.diff(data.counts))
    ok      = diff < threshold
    counts  = data.counts .where(ok, np.nan)
    bkgsub  = data.bkgsub .where(ok, np.nan)
    rate    = data.rate   .where(ok, np.nan)
    dquanta = data.dquanta.where(ok, np.nan)
    return data.assign(counts  = counts .fillna(method="ffill"),
                       bkgsub  = bkgsub .fillna(method="ffill"),
                       rate    = rate   .fillna(method="ffill"),
                       dquanta = dquanta.fillna(method="ffill"))

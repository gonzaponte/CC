from CCfun import *

import os
import string
import collections

import numpy as np

import pytest
import flaky


TEST_DATA_DIR = "/Users/Gonzalo/github/CC/test_data"

Datafile = collections.namedtuple("DATA", "filename data")

@pytest.fixture
def bkg_100_file():
    filename = os.path.join(TEST_DATA_DIR, "bkg_100s.asc")
    return Datafile(filename, load_ccd_file(filename))


@pytest.fixture
def saturated_file():
    filename = os.path.join(TEST_DATA_DIR, "saturated.asc")
    return Datafile(filename, load_ccd_file(filename))


@pytest.fixture
def power_file():
    filename = os.path.join(TEST_DATA_DIR, "power_data.asc")
    return filename


@pytest.fixture
def testdata_folder():
    return os.path.join(TEST_DATA_DIR, "testdata_folder")


@pytest.fixture
def state_0123():
    filename = os.path.join(TEST_DATA_DIR, "state_0_crystal_1_ex_wl_2_exposure_3_signal.asc")
    return Datafile(filename, load_ccd_file(filename))


def test_try_to_cast():
    for integer in np.random.randint(0, 1000, 100):
        assert try_to_cast(str(integer)) == integer

    for floating in np.random.uniform(0, 1000, 100):
        assert try_to_cast(str(floating)) == floating

    for _ in range(100):
        chars = "".join([np.random.choice(list(string.ascii_lowercase)) for _ in range(np.random.randint(1, 10))])
        assert try_to_cast(str(chars)) == chars


@flaky.flaky(max_runs=3)
def test_compute_baseline_standard(N=1000):
    true_baseline = np.random.randint(3000, 4000)
    counts        = np.random.normal(0, 20, N).astype(int) + true_baseline
    baseline      = compute_baseline(counts)

    assert abs(baseline - true_baseline) < 3


@flaky.flaky(max_runs=3)
def test_compute_baseline_with_saturation():
    true_baseline  = np.random.randint(3000, 4000)
    counts         = np.random.normal(0, 20, 1000).astype(int) + true_baseline
    counts[50:500] = 5000
    baseline       = compute_baseline(counts)
    assert abs(baseline - true_baseline) < 3


@pytest.mark.parametrize("subtract_baseline", (True, False))
def test_load_ccd_file(state_0123, subtract_baseline):
    wls, counts, rate, meta = load_ccd_file(state_0123.filename,
                                            subtract_baseline = subtract_baseline)

    true_wls    =  np.arange(1600) +  100.123
    true_counts =  np.arange(1600) + 3210
    true_rate   = (np.arange(1600) + (0 if subtract_baseline else 3210)) / 3

    assert np.allclose(   wls, true_wls   )
    assert np.allclose(counts, true_counts)
    assert np.allclose(  rate, true_rate  )

    assert meta["Grating Blaze"             ] == "300nm"
    assert meta["Input Side Slit Width (um)"] ==  750
    assert meta["Temperature (C)"           ] ==  -80
    assert meta["Exposure Time (secs)"      ] ==    3


def test_load_ccd_files(state_0123):
    N         = 5
    filenames = (state_0123.filename,) * N

    wls, counts, rate, metas = load_ccd_files(filenames)

    true_wls    = np.arange(1600) +  100.123
    true_counts = np.arange(1600) + 3210
    true_rate   = np.arange(1600) / 3

    true_wls    = np.tile(true_wls   , (N, 1))
    true_counts = np.tile(true_counts, (N, 1))
    true_rate   = np.tile(true_rate  , (N, 1))

    assert np.allclose(   wls, true_wls   )
    assert np.allclose(counts, true_counts)
    assert np.allclose(  rate, true_rate  )

    for meta in metas:
        assert meta["Grating Blaze"             ] == "300nm"
        assert meta["Input Side Slit Width (um)"] ==  750
        assert meta["Temperature (C)"           ] ==  -80
        assert meta["Exposure Time (secs)"      ] ==    3


@pytest.mark.parametrize("pattern", (None, SIGNAL_PATTERN_1))
@pytest.mark.parametrize("true_exposure", (0.1, 1, 10, 100))
def test_metadata_from_filename_1(true_exposure, pattern):
    true_state    = np.random.randint(0, 10000)
    true_crystal  = np.random.randint(0,    12)
    true_ex_wl    = np.random.randint(0,  1000)

    filename = SIGNAL_TEMPLATE_1.format(state    = true_state   ,
                                        crystal  = true_crystal ,
                                        ex_wl    = true_ex_wl   ,
                                        exposure = true_exposure)
    metadata = metadata_from_filename(filename, pattern)

    assert metadata.state    == true_state
    assert metadata.crystal  == true_crystal
    assert metadata.ex_wl    == true_ex_wl
    assert metadata.exposure == true_exposure


@pytest.mark.parametrize("true_exposure", (0.1, 1, 10, 100))
def test_metadata_from_filename_2(true_exposure):
    true_state      = np.random.randint(0, 10000)
    true_crystal    = np.random.randint(0,    12)
    true_ex_wl      = np.random.randint(0,  1000)
    true_grating    = np.random.randint(1,     3)
    true_looparound = np.random.randint(0,     2, dtype=bool)

    filename = SIGNAL_TEMPLATE_2.format(state      = true_state     ,
                                        crystal    = true_crystal   ,
                                        ex_wl      = true_ex_wl     ,
                                        exposure   = true_exposure  ,
                                        grating    = true_grating   ,
                                        looparound = true_looparound)
    metadata = metadata_from_filename(filename, SIGNAL_PATTERN_2)

    assert metadata.state      == true_state
    assert metadata.crystal    == true_crystal
    assert metadata.ex_wl      == true_ex_wl
    assert metadata.exposure   == true_exposure
    assert metadata.grating    == true_grating
    assert metadata.looparound == true_looparound


@pytest.mark.parametrize("template", (SIGNAL_TEMPLATE_1, SIGNAL_TEMPLATE_2))
@pytest.mark.parametrize("counter", np.arange(10))
def test_state_counter(template, counter):
    filename = template.format(state      = counter,
                               crystal    =       0,
                               ex_wl      =       0,
                               exposure   =       0,
                               grating    =       0,
                               looparound =       0)
    assert state_counter(filename) == counter


def test_select_crystal_filenames(testdata_folder):
    for crystal in range(3):
        filenames = select_crystal_filenames(testdata_folder, crystal)
        assert len(filenames) == 12

        for i, filename in enumerate(filenames):
            j, k     = i // 3, i % 3
            metadata = metadata_from_filename(filename)

            assert metadata.state    == 12 * crystal + i
            assert metadata.crystal  == crystal
            assert metadata.ex_wl    == 100 + j * 100
            assert metadata.exposure == 10**(k - 1)


def test_grating_from_blaze():
    assert grating_from_blaze("300nm") is 1
    assert grating_from_blaze("500nm") is 2


@pytest.mark.parametrize("blaze", (300, 500, "300", "500", "100nm", "200nm", "400nm", "600nm"))
def test_grating_form_blaze_raises_ValueError(blaze):
    with pytest.raises(ValueError):
        grating_from_blaze(blaze)


def test_is_saturated_True_filename(saturated_file):
    filename, _ = saturated_file
    assert is_saturated(filename)


def test_is_saturated_True_counts(saturated_file):
    _, data = saturated_file
    assert is_saturated(data.raw )
    assert is_saturated(data.rate)


def test_load_power_file(power_file):
    t, p, mean, std = load_power_file(power_file)
    assert len(t) == len(p) == 62
    assert p.mean() == mean
    assert p. std() ==  std
    assert np.isclose(   mean, 3.7871615096774184e-06)
    assert np.isclose(    std, 9.8753152548451700e-10)
    assert np.isclose(p.max(), 3.7903355400000000e-06)
    assert np.isclose(p.min(), 3.7846653000000000e-06)


@flaky.flaky(max_runs=3)
def test_sensible_std():
    normal  = np.random.normal(0, 5, 900)
    prestd  = normal.std()
    data    = np.concatenate([[-20]*50, normal, [20]*50])
    print(np.percentile(data,  5))
    print(np.percentile(data, 95))
    poststd = sensible_std(data)
    assert np.isclose(poststd, prestd)


def test_combine_measurements(state_0123):
    N          = 5
    filenames  = (state_0123.filename,) * N
    all_counts = load_ccd_files(filenames).raw
    combined   = combine_measurements(all_counts)

    for counts in all_counts:
        assert np.allclose(counts, combined)


@pytest.mark.parametrize("rebin_factor", np.arange(2, 5))
def test_rebin_duplicate_and_rebin(rebin_factor):
    true_data = np.arange(10)
    data      = np.repeat(true_data, rebin_factor)
    rebinned  = rebin    (     data, rebin_factor)
    assert np.allclose(rebinned, true_data)


@pytest.mark.parametrize("rebin_factor", np.arange(2, 5))
def test_rebin_padding(rebin_factor):
    true_data = np.arange(10)
    data      = np.repeat(true_data, rebin_factor)
    true_data = np.append(true_data, 0)
    data      = np.append(     data, 0)
    rebinned  = rebin    (     data, rebin_factor)
    assert np.allclose(rebinned, true_data)


@flaky.flaky(max_runs=3)
def test_pull():
    x = np.random.normal(10, 2**-0.5, 10000)
    y = np.random.normal( 0, 2**-0.5, 10000)
    p = pull(x, y)
    assert abs(p.mean() - 10) < .5
    assert abs(p. std() -  1) < .1


def test_power_to_quanta_rate():
    p  = 1.602e-19
    wl = np.random.uniform(0, 1000)
    qr = power_to_quanta_rate(p, wl * 1239.841984)
    assert np.allclose(qr, wl)


@pytest.mark.parametrize("wavelength", (0, 1 / 3400e-7))
def test_raman_wavelength_raises_ZeroDivisionError(wavelength):
    with pytest.raises(ZeroDivisionError):
        raman_wavelength(wavelength)


def test_raman_wavelength():
    rwl = raman_wavelength((1 - 3400e-7)**-1)
    assert np.isclose(rwl - 1, 0, atol=1e-3)


@pytest.mark.parametrize("wavelength", tuple(np.random.randint(100, 1000) for _ in range(10)))
def test_raman_shift(wavelength):
    assert np.isclose(wavelength + raman_shift(wavelength), raman_wavelength(wavelength))



def test_raman_differential_cross_section_raises_ZeroDivisionError():
    wavelength = 1e7 / 88e3
    with pytest.raises(ZeroDivisionError):
        raman_differential_cross_section(wavelength)


@pytest.mark.parametrize(" wavelength         xsection      ".split(),
                         ((       123, 1.5259239234467844e-25),
                          (      1234, 4.4255005890610250e-32),
                          (  1e7/3400, 0                     )))
def test_raman_differential_cross_section(wavelength, xsection):
    xsec = raman_differential_cross_section(wavelength)
    assert np.isclose(xsec, xsection, atol=0)


@pytest.mark.parametrize("N", (1, 2, 3))
def test_load_as_df(state_0123, N):
    filenames = (state_0123.filename,) * N
    dataframe = load_as_df(filenames)

    true_state      = np.full(          1600 * N,      0)
    true_crystal    = np.full(          1600 * N,      1)
    true_grating    = np.full(          1600 * N,      1)
    true_slit_width = np.full(          1600 * N,    750)
    true_exposure   = np.full(          1600 * N,      3)
    true_baseline   = np.full(          1600 * N,   3210)
    true_ex_wl      = np.full(          1600 * N,      2)
    true_power      = np.full(          1600 * N,   1230)
    true_em_wl      = np.tile(np.arange(1600), N) +  100.123
    true_counts     = np.tile(np.arange(1600), N) + 3210
    true_rate       = np.tile(np.arange(1600), N) /    3

    assert np.all(dataframe.state      == true_state     )
    assert np.all(dataframe.crystal    == true_crystal   )
    assert np.all(dataframe.grating    == true_grating   )
    assert        dataframe.looparound.isna().all()
    assert np.all(dataframe.slit_width == true_slit_width)
    assert np.all(dataframe.exposure   == true_exposure  )
    assert np.all(dataframe.baseline   == true_baseline  )
    assert np.all(dataframe.ex_wl      == true_ex_wl     )
    assert np.all(dataframe.em_wl      == true_em_wl     )
    assert np.all(dataframe.counts     == true_counts    )
    assert np.all(dataframe.rate       == true_rate      )
    assert np.all(dataframe.power      == true_power     )


@pytest.mark.parametrize("       expression              value        ".split(),
                         (("1 + 2 * np.exp(3) - 4", 37.171073846375336),
                          ("TEST_DATA_DIR"        , TEST_DATA_DIR     )))
def test_temp_var_values(expression, value):
    expected_value = eval(expression)
    with temp_var(expected_value) as value:
        assert value == expected_value

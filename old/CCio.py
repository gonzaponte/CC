import os
import operator
import datetime
import itertools
import collections

import numpy  as np
import tables as tb
import pandas as pd

from CCfun   import try_to_cast
from CCfun   import grating_from_blaze
from CCfun   import select_background_filenames
from CCfun   import select_ambient_filenames
from CCfun   import select_baseline_filenames
from CCfun   import select_crystal_filenames
from CCfun   import metadata_from_filename
from CCfun   import add_missing_backgrounds
from CCfun   import add_missing_exposures
from CCfun   import add_missing_baselines
from CCfun   import add_crystal_meta
from CCfun   import compute_baseline_int
from CCfun   import power_to_quanta_rate
from CCfun   import process
from CCfun   import crystal_type
from CCfun   import datetime_from_spectrum

from CCutils import map_keys
from CCutils import map_keys_values

from CCtypes import Pwr_data
from CCtypes import CCD_data
from CCtypes import Measurement
from CCtypes import Experiment

from CCconst import EMPTY_ARRAY
from CCconst import EMPTY_DF
from CCconst import TYPE_MAPPING


_compression_options = {
"NOCOMPR": (0,  None)        ,
"ZLIB1"  : (1, 'zlib')       ,
"ZLIB4"  : (4, 'zlib')       ,
"ZLIB5"  : (5, 'zlib')       ,
"ZLIB9"  : (9, 'zlib')       ,
"BLOSC5" : (5, 'blosc')      ,
"BLZ4HC5": (5, 'blosc:lz4hc'),
}

_default_sort = ["crystal_type", "crystal_id", "exposure",
                 "exp_index", "grating", "ex_wl", "em_wl"]


_default_power_cut = 0.9

################################################
# Write
################################################

def define_compression(label):
    if label not in _compression_options:
        raise ValueError(f"Compression option {label} not recognized")

    level, lib = _compression_options[label]
    return tb.Filters(complevel=level, complib=lib)


zlib4_compression = define_compression("ZLIB4")


auto = itertools.count(0).__next__
class SpectraTable(tb.IsDescription):
    full_scan  = tb.   BoolCol(pos = auto())
    crystal    = tb.   Int8Col(pos = auto())
    grating    = tb.   Int8Col(pos = auto())
    exp_index  = tb.   Int8Col(pos = auto())
    slit_width = tb.  Int16Col(pos = auto())
    state      = tb.  Int32Col(pos = auto())
    exposure   = tb.Float16Col(pos = auto())
    ex_wl      = tb.Float16Col(pos = auto())
    power      = tb.Float64Col(pos = auto())
    timestamp  = tb.Float64Col(pos = auto())
    em_wl      = tb.Float16Col(pos = auto())
    counts     = tb.  Int32Col(pos = auto())


################################################
# Read
################################################
def load_pwr_file(filename):
    data   = np.loadtxt(filename).T
    wl     = data[0 ].astype(int)
    means  = data[1:].mean(axis=0)
    stds   = data[1:].std (axis=0)
    ratios = stds / means
    return wl, means, stds, ratios


def load_pwr_files(filenames):
    wl, means, stds, ratios = [], [], [], []
    for filename in filenames:
        wl, m, s, r = load_pwr_file(filename)
        means  = np.append( means, m)
        stds   = np.append(  stds, s)
        ratios = np.append(ratios, r)

    shape = len(filenames), len(wl)
    return wl, means.reshape(shape) * 1e3, stds.reshape(shape) * 1e3, ratios.reshape(shape)


def load_power_file(filename,  power_cut=_default_power_cut):
    filename      = filename.replace("_signal", "_power")
    times, powers = np.loadtxt(filename, delimiter=",").T
    sel           = powers >  power_cut * powers.max()
    times         = times [sel]
    powers        = powers[sel]
    return Pwr_data(times, powers, powers.mean(), powers.std())


def load_double_power_file(filename,  power_cut=_default_power_cut):
    filename_a    = filename.replace("_signal", "_power_crystal")
    filename_b    = filename.replace("_signal", "_power_sample")
    output = []
    for filename in (filename_a, filename_b):
        try:
            times, powers = np.loadtxt(filename, delimiter=",").T
            sel           = powers >  power_cut * powers.max()
            times         = times [sel]
            powers        = powers[sel]
            output.append(Pwr_data(times, powers, powers.mean(), powers.std()))
        except:
            output.append(None)
    return output

def load_power_file_safe(filename,  power_cut=_default_power_cut):
    try                     : return load_double_power_file(filename,  power_cut)
    except Exception as e: print(e)
    except           OSError: return (Pwr_data(EMPTY_ARRAY, EMPTY_ARRAY, 0, 0),) * 2
    except FileNotFoundError: return (Pwr_data(EMPTY_ARRAY, EMPTY_ARRAY, 0, 0),) * 2
    except        ValueError: return (Pwr_data(EMPTY_ARRAY, EMPTY_ARRAY, 0, 0),) * 2


def read_experiment_metadata(folder):
    exec(open(os.path.join(folder, "metadata.txt")).read())
    return locals()["meta"]

def read_metadata(filename):
    metadata = {}
    for k, line in enumerate(open(filename)):
        if line == "\n": break
        name, *data    = line.rstrip().split(":")
        data           = ":".join(data)
        metadata[name] = try_to_cast(data.strip())
    return metadata, k+1

def load_ccd_try(filename, skiprows=28):
    try   :
        return np.loadtxt(filename, skiprows=skiprows).T
    except:
        print(f"{filename} could not be loaded")
        raise


def load_ccd_file(filename,  power_cut=_default_power_cut):
    metadata, header_length = read_metadata(filename)
    data        = load_ccd_try(filename, header_length)
    wl          = data[0]
    counts      = data[1:] if len(data) > 2 else data[1]
    powers      = load_power_file_safe(filename,  power_cut)
    baseline    = compute_baseline_int(counts)

    metadata["baseline"  ] = baseline
    metadata["grating_no"] = grating_from_blaze(metadata["Grating Blaze"])
    bls                    = counts - baseline
    rate                   = bls / metadata["Exposure Time (secs)"]
    return CCD_data(wl, counts, bls, rate, powers, metadata)


def load_ccd_files(filenames):
    wls       = []
    counts    = []
    blss      = []
    rate      = []
    powers    = []
    metadatas = []
    for filename in filenames:
        spectrum  = load_ccd_file(filename)
        wls       = np.append(wls       , spectrum.em_wl )
        counts    = np.append(counts    , spectrum.raw   )
        blss      = np.append(blss      , spectrum.bls   )
        rate      = np.append(rate      , spectrum.rate  )
        powers    = np.append(powers    , spectrum.power )
        metadatas     .append(            spectrum.meta  )

    shape  = len(filenames), len(wls) // len(filenames)
    wls    = wls   .reshape(shape)
    counts = counts.reshape(shape)
    blss   = blss  .reshape(shape)
    rate   = rate  .reshape(shape)
    powers = powers.reshape(shape)
    return CCD_data(wls, counts, blss, rate, powers, metadatas)


def load_backgrounds(folder):
    filenames = select_background_filenames(folder)
    spectra   = collections.defaultdict(dict)

    for filename in filenames:
        meta     = metadata_from_filename(filename)
        spectrum = load_ccd_file(filename)
        spectra[meta.crystal][meta.exposure][meta.when] = spectrum

    return spectra


def load_ambients(folder):
    filenames = select_ambient_filenames(folder)
    spectra   = collections.defaultdict(lambda: collections.defaultdict(dict))

    for filename in filenames:
        meta     = metadata_from_filename(filename)
        spectrum = load_ccd_file(filename)
        spectra[meta.crystal][meta.exposure][meta.when] = spectrum

    return spectra


def load_baselines_as_df(folder):
    filenames = select_baseline_filenames(folder)
    dfs       = []

    for index, filename in enumerate(sorted(filenames)):
        meta     = metadata_from_filename(filename)
        spectrum = load_ccd_file         (filename)

        dfs.append(pd.DataFrame(dict(em_wl     = spectrum.em_wl,
                                     counts    = spectrum.raw  ,
                                     crystal   = meta.crystal  ,
                                     full_scan = meta.full_scan,
                                     counter   = meta.state)))

    if dfs:
        return pd.concat(dfs, ignore_index = True)

    return pd.DataFrame(dict(em_wl     = [],
                             counts    = [],
                             crystal   = [],
                             full_scan = [],
                             counter   = []))


def load_as_df(filenames, power_cut=_default_power_cut):
    dfs         = [EMPTY_DF]
    exp_index   = dict()

    for filename in filenames:
        try:
            spectrum   = load_ccd_file(filename, power_cut)
        except Exception as e:
            print(f"Could not load {filename}")
            print(f"Error: {e}")
            continue
        power      = spectrum.power[1].mean
        meta       = metadata_from_filename(filename)
        length     = len(spectrum.raw)
        baseline   = spectrum.meta["baseline"                  ]
        slit_width = spectrum.meta["Input Side Slit Width (um)"]
        timestamp  = datetime_from_spectrum(spectrum).timestamp()
        quanta_in  = power_to_quanta_rate(power, meta.ex_wl)
        dquanta    = spectrum.rate / quanta_in

        try   : indices    = meta.full_scan , meta.crystal, meta.ex_wl, meta.mono_grating, meta.exposure
        except: indices    = meta.looparound, meta.crystal, meta.ex_wl, meta.     grating, meta.exposure
        exp_idx    = exp_index.get(indices, 0)
        exp_index[indices] = exp_idx + 1

        try   : grating = meta.mono_grating
        except: grating = meta.     grating

        try   : scan = meta.full_scan
        except: scan = meta.looparound

        dfs.append(pd.DataFrame(dict(state      = [meta.state       ] * length,
                                     timestamp  = [timestamp        ] * length,
                                     crystal    = [meta.crystal     ] * length,
                                     grating    = [grating          ] * length,
                                     full_scan  = [scan             ] * length,
                                     slit_width = [slit_width       ] * length,
                                     exposure   = [meta.exposure    ] * length,
                                     exp_index  = [exp_idx          ] * length,
                                     baseline   = [baseline         ] * length,
                                     ex_wl      = [meta.ex_wl       ] * length,
                                     em_wl      = spectrum.em_wl              ,
                                     counts     = spectrum.raw                ,
                                     rate       = spectrum.rate               ,
                                     power      = [power            ] * length,
                                     dquanta    = dquanta                     )))
    return pd.concat(dfs, ignore_index=True, sort=True)


def load_folder(folder):
    dfs = []
    for i in range(12):
        filenames = select_crystal_filenames(folder, i)
        df        = load_as_df(filenames)
        dfs.append(df)
    return pd.concat(dfs)


def read_df_native_types(records):
    df = pd.DataFrame.from_records(records)
    for col_name, col_type in df.dtypes.items():
        if col_type.name not in TYPE_MAPPING: continue

        new_type = TYPE_MAPPING[col_type.name]
        df.loc[:, col_name  ] = df.loc[:, col_name].astype(new_type)

    df.loc[:, "exposure"] = df.exposure.apply(np.round, decimals=3)
    if "timestamp" in df.columns:
        df.loc[:, "timestamp"] = df.timestamp.map(datetime.datetime.fromtimestamp)
    return df


def load_references_from_h5(filename):
    with tb.open_file(filename) as file:
        dfs = map(read_df_native_types,
                  map(tb.Table.read, file.root.Data.References))

        return pd.concat(list(dfs), ignore_index=True)


def load_metadata_from_h5(filename):
    with tb.open_file(filename) as file:
        meta  = file.root.Data.Scans.Crystals.attrs.metadata
        meta["crystal_mapping"     ] = map_keys       (meta["crystal_mapping"], int)
        meta["crystal_type_mapping"] = map_keys_values(meta["crystal_mapping"], int, crystal_type)
        return meta


def load_crystals_from_h5(filename):
    with tb.open_file(filename) as file:
        table = file.root.Data.Scans.Crystals
        df    = read_df_native_types(table.read())
        return df


def load_backgrounds_from_h5(filename, which="Before"):
    with tb.open_file(filename) as file:
        table = getattr(file.root.Data.Backgrounds, which)
        return read_df_native_types(table.read())


def load_baselines_from_h5(filename):
    with tb.open_file(filename) as file:
        if "Baselines" in file.root.Data.Scans:
            table = file.root.Data.Scans.Baselines
            return read_df_native_types(table.read())


def load_and_process(filename, wheel_number=0):
    meta = load_metadata_from_h5    (filename)
    df   = load_crystals_from_h5    (filename)
    ref  = load_references_from_h5  (filename)
    bsl  = load_baselines_from_h5   (filename)
    bkg  = load_backgrounds_from_h5 (filename)
    df   = add_crystal_meta         (df, meta)
    process(df, bsl)

    return Measurement( df.assign(wheel_no = wheel_number),
                       ref.assign(wheel_no = wheel_number),
                       bkg.assign(wheel_no = wheel_number),
                       meta,
                       bsl.assign(wheel_no = wheel_number))


def load_prepost(filename_pre, filename_post):
    pre  = load_and_process(filename_pre )
    post = load_and_process(filename_post)
    return Experiment(pre, post)


def merge_measurements(measurements, sort_by=None):
    df, ref, bkg, meta, bsl = zip(*measurements)
    df    = pd.concat(df , ignore_index=True)
    ref   = pd.concat(ref, ignore_index=True)
    bkg   = pd.concat(bkg, ignore_index=True)
    bsl   = pd.concat(bsl, ignore_index=True)

    if sort_by:
        df = df.sort_values(sort_by)
    return Measurement(df, ref, bkg, meta, bsl)


def merge_experiments(experiments, sort_by=None):
    pres  = map(operator.attrgetter("pre") , experiments)
    posts = map(operator.attrgetter("post"), experiments)
    pre   = merge_measurements(pres , sort_by)
    post  = merge_measurements(posts, sort_by)
    return Experiment(pre, post)


def load_many(filenames, start=0):
    return merge_measurements(map(load_and_process, filenames, range(start, start + len(filenames))))


def load_all(filenames_pre, filenames_post, sort_by=_default_sort):
    n_pre = len(filenames_pre )
    pre   = load_many(filenames_pre ,     0)
    post  = load_many(filenames_post, n_pre)
    return Experiment(pre, post)


def map_crystals(experiment, column_name, mapping):
    for df in (experiment.pre.crystals, experiment.post.crystals):
        df[column_name] = df.crystal_id.map(mapping)

from collections import namedtuple

CCD_data    = namedtuple("CCD_DATA"   , "em_wl raw bls rate power meta")
Pwr_data    = namedtuple("PWR_DATA"   , "time power mean std")
Meta_data   = namedtuple("META_DATA"  , "state crystal ex_wl exposure grating looparound when")
Meta_data2  = namedtuple("META_DATA"  , "state crystal ex_wl exposure mono_grating full_scan when")
Measurement = namedtuple("Measurement", "crystals references background metadata baselines")
Experiment  = namedtuple("Experiment" , "pre post")
tagged_file = namedtuple("TAGGEDFILE" , "timestamp filename")

Meta_data  .__new__.__defaults__ = (None,) * 7
Meta_data2 .__new__.__defaults__ = (None,) * 7
Measurement.__new__.__defaults__ = (None,) * 5

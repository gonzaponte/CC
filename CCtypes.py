from typing import Union

Value = Union[int, float, str, bool]


class Namespace:
    def __init__(self, **kwargs):
        self.update(kwargs)

    def update(self, name, value=None):
        if value is None: # assume it's a mapping
            self.__dict__.update(name)
        else:
            self.__dict__.update({str(name) : value})

    def rename(self, oldname, newname):
        value = self[oldname]
        del self.__dict__[oldname]
        self.update(newname, value)

    def __or__(self, other):
        new = Namespace(**self.__dict__)
        new.update(other.__dict__ if isinstance(other, Namespace) else other)
        return new

    def __str__(self):
        out = "Namespace(\n"
        for name, value in self.__dict__.items():
            if name.startswith("_") or name == "update": continue

            out += f"         | {repr(name):<30} -> {repr(value):>30}\n"
        out += "         )\n"
        return out

    __repr__ = __str__

    def __getitem__(self, key):
        return self.__dict__[key]

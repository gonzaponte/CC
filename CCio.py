import os

from glob      import glob
from functools import wraps
from functools import partial
from functools import lru_cache
from datetime  import datetime

import numpy  as np
import pandas as pd
import tables as tb

from CCtypes   import Namespace

from CCcore    import ccd_datetime
from CCcore    import try_to_cast
from CCcore    import _default_power_cut
from CCcore    import get_power_files
from CCcore    import process_power
from CCcore    import power_to_quanta_rate
from CCcore    import concat

from CCdb      import grating_from_blaze

from typing import Union
from typing import Sequence


_compression    = tb.Filters(complevel=4, complib="zlib")
_str_col_length = 100


def load_power_file(filename : str) -> (np.ndarray, np.ndarray):
    times, powers = np.loadtxt(filename, delimiter=",").T
    return times, powers


def load_metadata_from_ccd(filename : str) -> Namespace:
    meta = Namespace()
    for line in open(filename):
        if line == "\n": break

        name, data = line.rstrip().split(":", maxsplit=1)

        name = name.split("(")[0].strip().replace(" ", "_")

        meta.update(name, try_to_cast(data.strip()))

    meta.rename("Input_Side_Slit_Width", "slit_width"        )
    meta.rename("Wavelength"           , "spectro_wavelength")
    meta.rename("Exposure_Time"        , "exp_time"          )

    meta.update("spectro_grating", grating_from_blaze(meta.Grating_Blaze))
    meta.update("datetime"       , ccd_datetime      (meta.Date_and_Time))
    return meta


def load_metadata_from_filename(filename : str) -> Namespace:
    meta  = Namespace()

    tokens = os.path.basename(filename).split("_")

    for name, value in zip(tokens[0::2], tokens[1::2]):
        if name in ("power", "signal"): break
        meta.update(name, try_to_cast(value))

    meta.rename("exwl"       , "ex_wl"       )
    meta.rename("monograting", "mono_grating")
    return meta


@lru_cache(maxsize=1)
def load_metadata_from_folder(folder : str) -> Namespace:
    # Defines variable meta
    exec(open(os.path.join(folder, "metadata.txt")).read())
    metadata   = locals()["meta"]
    crystal_id = next(iter(metadata["crystal_mapping"].values()))
    return Namespace(crystal_id = crystal_id)


def load_metadata(filename : str) -> Namespace:
    meta1 = load_metadata_from_ccd(filename)
    meta2 = load_metadata_from_filename(filename)
    meta3 = load_metadata_from_folder(os.path.dirname(filename))
    return meta1 | meta2 | meta3


def load_spectrum_as_array(filename : str) -> (np.ndarray, np.ndarray):
    skiprows = 1
    for line in open(filename):
        if line == "\n": break
        skiprows += 1

    return np.loadtxt(filename, skiprows=skiprows).T


def load_spectrum_as_df(filename : str) -> pd.DataFrame:
    wls, counts = load_spectrum_as_array(filename)
    return pd.DataFrame(dict(em_wl=wls, counts=counts))


def concat_output(pattern):
    def decorator(f):
        @wraps(f)
        def decorated(filenames_or_folder, *args, **kwargs):
            if not isinstance(filenames_or_folder, str):
                filenames = filenames_or_folder
                dfs = [f(filename, *args, **kwargs) for filename in filenames]
                return concat(dfs)

            if os.path.isdir(filenames_or_folder):
                if not isinstance(pattern, str):
                    msg = "Function called with an empty pattern on a folder"
                    raise ValueError(msg)
                filenames = glob(os.path.join(filenames_or_folder, pattern))
                return decorated(filenames, *args, **kwargs)

            # it's a single file
            filename = filenames_or_folder
            return f(filename, *args, **kwargs)

        return decorated

    return decorator if isinstance(pattern, str) else decorator(pattern)


@concat_output("*signal*")
def load_processed( filename : str
                  , baseline : int   = 0
                  , cut      : float =_default_power_cut
                  ) -> pd.DataFrame:
    df   = load_spectrum_as_df(filename)
    meta = load_metadata      (filename)
    meta.update("baseline", baseline)

    bls  = df.counts - baseline
    rate = bls / meta.exposure

    powers = {}
    dqs    = {}
    for i, pfile in enumerate(get_power_files(filename)):
        _, power = process_power(*load_power_file(pfile), cut)
        powers[f"power{i}"] = power
        dqs   [f"dq{i}"   ] = rate / power_to_quanta_rate(power, meta.ex_wl)

    return df.assign( state        = meta.state
                    , datetime     = meta.datetime
                    , timestamp    = meta.datetime.timestamp()
                    , grating_mono = meta.mono_grating
                    , grating_spec = meta.spectro_grating
                    , crystal      = meta.crystal
                    , crystal_id   = meta.crystal_id
                    , ex_wl        = meta.ex_wl
                    , exposure     = meta.exposure
                    , bls          = bls
                    , rate         = rate
                    , **powers
                    , **dqs
                    )


def load_baseline( filename : str
                 , timeref  : Union[float, datetime, None] = None
                 ) -> pd.DataFrame:
    baselines = pd.read_hdf(filename, "Baseline")

    if timeref is None:
        return baselines

    if isinstance(timeref, datetime):
        timeref = timeref.timestamp()

    groups     = baselines.groupby("timestamp")
    timestamps = np.asarray(list(groups.groups))
    deltas     = np.abs(timestamps - timeref)
    index      = np.argmin(deltas)
    return groups.get_group(timestamps[index])


def load_node( filename : str
             , group    : str
             , node     : str
             , **kwargs
             ) -> pd.DataFrame:
    key = f"/{node}" if group is None else f"/{group}/{node}"
    df = pd.read_hdf(filename, key, **kwargs)

    read_timestamp = np.vectorize(datetime.fromtimestamp)
    read_date      = np.vectorize(datetime.date)
    if "timestamp" in df.columns:
        datetimes = read_timestamp(df.timestamp)
        dates     = read_date     (datetimes)
        df = df.assign( datetime = datetimes
                      , date     = dates
                      )

    return df


def get_group(file : tb.file.File, group : Union[str, None]):
    return file.root if group is None else getattr(file.root, group)


@concat_output("*.h5")
def load_hdf( filename : str
            , group    : Union[str, None] = None
            , nodes    : Union[str, Sequence[str], None] = None
            , **kwargs
            ) -> pd.DataFrame:
    if nodes is None or nodes == "*":
        with open_file(filename) as file:
            nodes = tuple(node.name for node in get_group(file, group))

    if isinstance(nodes, str):
        nodes = nodes,

    return concat([load_node(filename, group, node) for node in nodes])


open_file = partial(tb.open_file, filters=_compression)


def _make_tabledef(column_types : np.dtype) -> dict:
    tabledef = {}
    for indx, colname in enumerate(column_types.names):
        coltype = column_types[colname].name
        if coltype == 'object':
            tabledef[colname] = tb.StringCol(_str_col_length, pos=indx)
        else:
            tabledef[colname] = tb.Col.from_type(coltype, pos=indx)
    return tabledef


def df_writer( df    : pd.DataFrame
             , file  : Union[tb.file.File, str]
             , group : Union[str, None]
             , node  : str
             , mode  : str = None
             ) -> None:

    is_filename = isinstance(file, str)
    if is_filename:
        file = open_file(file, mode)

    if group is None:
        group = file.root
    elif group not in file.root:
        group = file.create_group(file.root, group)
    else:
        group = getattr(file.root, group)

    data = df.to_records(index=False)

    if node not in group:
        tabledef = _make_tabledef(data.dtype)
        table    = file.create_table(group, node, tabledef, "", _compression)
    else:
        table = getattr(group, node)

    data_types = table.dtype
    if len(data) != 0:
        columns = list(data_types.names)
        data    = data[columns].astype(data_types)
        table.append(data)
        table.flush()

    if is_filename:
        file.close()

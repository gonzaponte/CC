import os

from glob       import glob
from datetime   import datetime
from subprocess import check_output

import numpy  as np
import pandas as pd

from typing import Sequence
from typing import Mapping
from typing import Callable

from CCtypes import Value


_default_power_cut = 0.9


def check_size(path : str) -> str:
    call = check_output(f"du -hsc {path}".split())
    return call.decode().split("\t")[0]


def in_range( x    :Sequence
            , low  : float = None
            , high : float = None
            ) -> np.ndarray:

    x   = np.asarray(x)
    sel = np.ones(len(x), dtype=bool)
    if low  is not None: sel &= x >= low
    if high is not None: sel &= x <  high
    return sel


def centers(x : Sequence) -> Sequence:
    x = np.asarray(x)
    return x[:-1] + np.diff(x) / 2


def edges(x : Sequence) -> Sequence:
    left  = x[ 0] - np.diff(x[  :2]) / 2
    right = x[-1] + np.diff(x[-2: ]) / 2
    return np.concatenate([left, centers(x), right])


def histogram(*args, **kwargs) -> (np.ndarray, np.ndarray):
    heights, edges = np.histogram(*args, **kwargs)
    bin_centers = centers(edges)
    return heights, bin_centers


def merge_dicts(*ds : Mapping) -> Mapping:
    combined = {}
    for d in ds:
        combined.update(d)
    return combined


def df_mask( df                 : pd.DataFrame
           , selections         : Mapping[str, Value] = dict()
           , skip_failures      : bool                = False
           , **other_selections : Mapping[str, Value]
           ) -> np.ndarray:
    selections = merge_dicts(selections, other_selections)

    mask = np.ones(len(df), dtype=bool)
    for column, value in selections.items():
        try:
            mask &= df.loc[:, column] == value
        except:
            if not skip_failures:
                raise
    return mask


def filter_df( df                 : pd.DataFrame
             , selections         : Mapping[str, Value] = dict()
             , skip_failures      : bool                = False
             , **other_selections : Mapping[str, Value]
             ) -> pd.DataFrame:
    selection = df_mask(df, selections, skip_failures, **other_selections)

    if not np.any(selection):
        raise RuntimeError("Empty DataFrame")

    return df.loc[selection]


def try_to_cast(value : str) -> Value:
    try                  : return int  (value)
    except ValueError    :
        try              : return float(value)
        except ValueError: return (True  if value.lower() == "true"  else
                                   False if value.lower() == "false" else value)


def ccd_datetime(timestr : str) -> datetime:
    date = timestr.split(".")[ 0]
    year = timestr.split(" ")[-1]
    date = f"{date} {year}"
    return datetime.strptime(date, "%c")


def date_from_folder(folder : str) -> datetime:
    datestr = " ".join(os.path.basename(folder).split("_")[-7:-1])
    return datetime.strptime(datestr, "%Y %m %d %H %M %S")


def timestamp_from_folder(folder : str) -> float:
    return date_from_folder(folder).timestamp()


def get_power_files(filename : str) -> Sequence[str]:
    valid = []
    for filename in sorted(glob(filename.replace("_signal", "_power*"))):
        if open(filename).readline() != "":
            valid.append(filename)

    return valid


def process_power( times  : np.ndarray
                 , powers : np.ndarray
                 , cut    : float      = _default_power_cut
                 ) -> (float, float):
    sel = powers > powers.max() * cut
    if np.any(sel):
        return times[sel].mean(), powers[sel].mean()
    else:
        return times.mean(), powers.mean()


def power_to_quanta_rate(power : float, ex_wl : float) -> float:
    quanta_rate  = power / 1.602e-19   # eV/s
    quanta_rate *= ex_wl / 1239.841984 # photons/s
    return quanta_rate


def average_baselines(df : pd.DataFrame) -> pd.DataFrame:
    n = len(df) // len(df.timestamp.drop_duplicates())

    pixel_index = np.vectorize(lambda x: x % n)
    return df.groupby(pixel_index).mean()


def rebin_array( data : Sequence
               , n    : int      = 2
               , op   : Callable = np.sum
               ) -> np.ndarray:
    data = np.asarray(data)
    op   = np.vectorize(op)
    if data.size % n:
        npad = n - data.size % n
        pad  = [data[-1]] * npad
        data = np.append(data, pad)

    return op(data.reshape(data.size // n, n), axis=1)


def rebin_df( df : pd.DataFrame
            , n  : int = 2
            ) -> pd.DataFrame:
    @np.vectorize
    def grouper(x):
        return x // n

    return df.groupby(grouper).agg(np.mean).reset_index()


def integrate_peak( df              : pd.DataFrame
                  , min             : float
                  , max             : float
                  , column          : str = "counts"
                  , remove_baseline : bool = True
                  ):
    column   = getattr(df, column).loc[in_range(df.em_wl, min, max)]
    baseline = column.min() if remove_baseline else 0
    return (column - baseline).sum()


def fix_hot_pixel(df, index=390):
    rindex = df.index % 2048
    before = df.loc[rindex == index - 1]
    after  = df.loc[rindex == index + 1]
    tofix  =        rindex == index
    for col in "counts bls rate dq0 dq1".split():
        if col not in df.columns: continue
        df.loc[tofix, col] = before[col].values
    return df


def raman_differential_cross_section(wavelength : float) -> float:
    A    = 5.33e-27         # cm2 / sr / molecule
    nu_i = 88e3             # cm-1
    nu_p = 1e7 / wavelength # cm-1
    nu_s = nu_p - 3400      # cm-1
    return A * nu_s**4 / (nu_i**2 - nu_p**2)**2


def raman_wavelength(wavelength : float) -> float:
    return 1 / (1 / wavelength - 3400e-7)


def raman_shift(wavelength : float) -> float:
    return raman_wavelength(wavelength) - wavelength


def concat(items, reset_index=True, **kwargs):
    df = pd.concat(items, ignore_index=True, sort=False, **kwargs)
    return df.reset_index(drop=True) if reset_index else df


def remove_duplicate_grating(df, drop=1):
    sel = (df.ex_wl == 400) & (df.grating_mono == drop)
    return df.loc[~sel].reset_index(drop=True)

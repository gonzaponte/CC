import os
import cycler
import contextlib

from scipy.interpolate import griddata

import matplotlib
import matplotlib.pyplot as plt
import numpy             as np

from CCcore import in_range
from CCcore import edges


color_sequence = ("k", "m", "g", "b", "r",
                  "gray", "aqua", "gold", "lime", "purple",
                  "brown", "lawngreen", "tomato", "lightgray", "lightpink")

figure_sizes = {
    1 : ( 8,  6),
    2 : (16,  6),
    3 : (24,  6),
    4 : (16, 12),
    5 : (24, 12),
    6 : (24, 12),
    7 : (24, 18),
    8 : (24, 18),
    9 : (24, 18),
   10 : (16, 30),
   12 : (24, 24),
}

subplots = {
    1 : (1, 1),
    2 : (1, 2),
    3 : (1, 3),
    4 : (2, 2),
    5 : (2, 3),
    6 : (2, 3),
    7 : (3, 3),
    8 : (3, 3),
    9 : (3, 3),
   10 : (5, 2),
   12 : (4, 3),
}

def change_jupyter_width(width):
    s  = "from IPython.core.display import display, HTML;"
    s += "display(HTML('<style>.container { width:" + str(width) + "% !important; }</style>'))"
    return s # use exec(s)

def auto_plot_style(overrides = dict()):
    plt.rcParams[ "figure.figsize"               ] = 8, 6
    plt.rcParams[   "font.size"                  ] = 20
    plt.rcParams[  "lines.markersize"            ] = 15
    plt.rcParams[  "lines.linewidth"             ] = 3
    plt.rcParams[  "patch.linewidth"             ] = 3
    plt.rcParams[   "axes.linewidth"             ] = 2
    plt.rcParams[   "grid.linewidth"             ] = 3
    plt.rcParams[   "grid.linestyle"             ] = "--"
    plt.rcParams[   "grid.alpha"                 ] = 0.5
    plt.rcParams["savefig.dpi"                   ] = 300
    plt.rcParams["savefig.bbox"                  ] = "tight"
    plt.rcParams[   "axes.formatter.use_mathtext"] = True
    plt.rcParams[   "axes.formatter.limits"      ] = (-3 ,4)
    plt.rcParams[   "axes.prop_cycle"            ] = cycler.cycler(color=color_sequence)
    plt.rcParams[  "image.cmap"                  ] = "viridis"
    plt.rcParams.update(overrides)


@contextlib.contextmanager
def temporary(name, new_value):
    old_value          = plt.rcParams[name]
    plt.rcParams[name] = new_value
    try    : yield
    finally: plt.rcParams[name] = old_value


def figure(n=1):
    return plt.figure(figsize=figure_sizes[n])


def subplot(a, b=None, c=None):
    if c is None:
        (nx, ny), k = subplots[a], b
    else:
        nx, ny, k = a, b, c
    return plt.subplot(nx, ny, k)


def labels(xlabel=None, ylabel=None, title=None):
    if xlabel is not None: plt.xlabel(xlabel)
    if ylabel is not None: plt.ylabel(ylabel)
    if title  is not None: plt. title( title)


def normhist(x, *args, **kwargs):
    w = np.full(len(x), 1/len(x))
    return plt.hist(x, *args, weights=w, **kwargs)


def plot1d( em_wls, cts, title="", *
          , autoyrange = False
          , ex_wl      = None
          , log        = False
          , ylabel     = "Counts"
          , drawstyle  = "steps"
          , **style
          ):
    style.update(dict(drawstyle=drawstyle))
    plot  = plt.plot(em_wls, cts, **style)
    plt.xlabel("Emmision wl (nm)")
    plt.ylabel(ylabel)
    plt.title (title)

    if autoyrange:
        if ex_wl is None:
            raise ValueError("You must provide a ex_wl value if autoyrange==True")

        ymin = np.min(cts) - 100
        ymax = np.max(cts[np.abs(em_wls - ex_wl) > 20]) + 100

        plt.ylim(ymin, ymax)

    if log:
        plt.yscale("log")

    return plot


def plot2d( ex_wls, em_wls, cts
          , title      = ""  , zlabel = "counts"
          , emwl_delta = 20  , log    = False
          , cmin       = None, cmax   = None
          , vmin       = None, vmax   = None
          , cmap       = None, **cbar):
    ex_wls = np.asarray(ex_wls)
    em_wls = np.asarray(em_wls)
    cts    = np.asarray(cts   )

    x    = np.arange  (ex_wls.min(), ex_wls.max() + 1)
    y    = np.linspace(em_wls.min(), em_wls.max(), 5000)
    x, y = np.meshgrid(x, y, indexing="ij")
    xr   = np.ravel(x)
    yr   = np.ravel(y)
    xy   = np.column_stack([xr, yr])
    zr   = griddata(np.column_stack([ex_wls, em_wls]), cts, xy)

    selection = True
    if emwl_delta > 0:
        selection &= yr - xr > emwl_delta

    if cmin is not None or cmax is not None:
        selection &= in_range(zr, cmin, cmax)

    zr[~selection] = np.nan
    z  = zr.reshape(x.shape)

    style = dict( vmin = np.nanmin(vmin) if vmin is None else vmin
                , vmax = np.nanmax(vmax) if vmax is None else vmax
                , norm = matplotlib.colors.LogNorm() if log else None
                , cmap = cmap)

    plot     = plt.pcolormesh(x, y, z, **style)
    colorbar = plt.colorbar(**cbar)
    colorbar.set_label(zlabel)
    plt.xlabel("Excitation wl (nm)")
    plt.ylabel("Emmision wl (nm)")
    plt.title (title)
    return plot, colorbar


def _plot2d(ex_wls, em_wls, cts, title="", emwl_delta=20, log=False, cmin=None, cmax=None, vmin=None, vmax=None):
    ex_wls = np.asarray(ex_wls)
    em_wls = np.asarray(em_wls)
    cts    = np.asarray(cts   )

    selection = True
    if emwl_delta > 0:
        selection &= em_wls - ex_wls > emwl_delta

    if cmin is not None or cmax is not None:
        selection &= in_range(cts, cmin, cmax)

    cts[~selection] = np.nan

    unique = np.unique(ex_wls)
    shape  = len(unique), len(em_wls) // len(unique)
    ex_wls = ex_wls.reshape(shape).T
    em_wls = em_wls.reshape(shape).T
    cts    =    cts.reshape(shape).T

    ex_wls = np.pad(ex_wls, ((0, 0), (0, 1)), mode="edge")
    em_wls = np.apply_along_axis(edges, 1, em_wls)

    style = dict( vmin = np.nanmin(vmin) if vmin is None else vmin
                , vmax = np.nanmax(vmax) if vmax is None else vmax
                , norm = matplotlib.colors.LogNorm() if log else None
                )

    scatter  = plt.pcolormesh(ex_wls, em_wls, cts, **style)
    colorbar = plt.colorbar()
    colorbar.set_label("counts")
    plt.xlabel("Excitation wl (nm)")
    plt.ylabel("Emmision wl (nm)")
    plt.title (title)
    return scatter, colorbar


def old_plot2d(ex_wls, em_wls, cts, title="", emwl_delta=20, log=False, cmin=0, cmax=2**16, vmin=None, vmax=None):
    if len(ex_wls) != len(em_wls):
        ex_wls = np.repeat(ex_wls, em_wls.shape[1])

    selection = (cts >= cmin) & (cts < cmax)
    if emwl_delta > 0:
        selection &= em_wls - ex_wls > emwl_delta

    ex_wls    = ex_wls[selection]
    em_wls    = em_wls[selection]
    cts       = cts   [selection]

    style = dict(marker = "s",
                 s      =  10,
                 vmin   = cts.min() if vmin is None else vmin,
                 vmax   = cts.max() if vmax is None else vmax)
    if log:
        style["norm"] = matplotlib.colors.LogNorm()

    scatter  = plt.scatter(ex_wls, em_wls, c=cts, **style)
    colorbar = plt.colorbar()
    colorbar.set_label("counts")
    plt.xlabel("Excitation wl (nm)")
    plt.ylabel("Emmision wl (nm)")
    plt.title (title)
    return scatter, colorbar


def old_plot2d_2(ex_wls, em_wls, cts, title="", emwl_delta=20, log=False, cmin=0, cmax=2**16, vmin=None, vmax=None):
    if len(ex_wls) != len(em_wls):
        ex_wls = np.repeat(ex_wls, em_wls.shape[1])

    selection = (cts >= cmin) & (cts < cmax)
    if emwl_delta > 0:
        selection &= em_wls - ex_wls > emwl_delta

    ex_wls    = ex_wls[selection]
    em_wls    = em_wls[selection]
    cts       = cts   [selection]

    ex_bins        = np.linspace(ex_wls.min(), ex_wls.max(),   51)
    em_bins        = np.linspace(em_wls.min(), em_wls.max(), 1001)
    ex_bin_centers = ex_bins[:-1] + np.diff(ex_bins)[0]
    em_bin_centers = em_bins[:-1] + np.diff(em_bins)[0]
    bin_centers    = np.meshgrid(ex_bin_centers, em_bin_centers, indexing="ij")
    bin_centers    = np.column_stack(tuple(map(np.ravel, bin_centers)))
    interpolated   = griddata( np.column_stack([ex_wls, em_wls])
                             , cts
                             , bin_centers
                             , method="linear"
                             )

    style = dict(vmin   = cts.min() if vmin is None else vmin,
                 vmax   = cts.max() if vmax is None else vmax,
                 norm   = matplotlib.colors.LogNorm() if log else None,
                 extent = [ex_bins[0], ex_bins[-1],
                           em_bins[0], em_bins[-1]])

    hist = plt.imshow(interpolated.reshape(len(ex_bin_centers), len(em_bin_centers)).T, aspect="auto", origin="lower", **style)

    colorbar = plt.colorbar()
    colorbar.set_label("counts")
    plt.xlabel("Excitation wl (nm)")
    plt.ylabel("Emmision wl (nm)")
    plt.title (title)
    return hist, colorbar



def plot_line(value, axis="y", *args, **kwargs):
    axis = axis.lower()
    if axis not in "x y".split():
        raise ValueError(f"Invalid value of argument axis: {axis}")

    if axis == "x":
        xvalues = plt.xlim()
        yvalues = np.array([value] * 2)
    else:
        xvalues = np.array([value] * 2)
        yvalues = plt.ylim()

    return plt.plot(xvalues, yvalues, *args, **kwargs)


def plot_saver(folder, extensions="png pdf".split(), **kwargs):
    default_args = dict(png = dict(dpi=400),
                        pdf = dict()       )

    if not os.path.exists(folder):
        os.mkdir(folder)

    def do_save(filename):
        for extension in extensions:
            args = dict(default_args.get(extension, dict()))
            args.update(kwargs)
            plt.savefig(os.path.join(folder, f"{filename}.{extension}"), **args)

    return do_save


def set_ylimits(data, factor=1.1):
    low   = min(data)
    low  *= factor if low  < 0 else 1/factor
    high  = max(data)
    high *= factor if high > 0 else 1/factor
    plt.ylim(low, high)

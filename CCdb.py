import os

import numpy as np

from scipy.interpolate import interp1d
from scipy.interpolate import interp2d

from typing import Callable
from typing import Union

DATABASE_DIR = os.path.join(os.environ["CCDIR"], "database")
SPECS_DIR    = os.path.join(DATABASE_DIR, "efficiency_digitization")

_default_power_ratio_file = os.path.join(DATABASE_DIR, "power_ratio.npy")
_default_reference_file   = os.path.join(DATABASE_DIR, "references.h5")


def load_spectral_radiance() -> Callable:
    filename = os.path.join(SPECS_DIR, "RadianceDigi.csv")
    wls, radiance = np.loadtxt(filename, delimiter = ",").T
    return interp1d(wls, radiance)


def load_grating2_efficiencies() -> (Callable, Callable, Callable):
    filename = os.path.join(SPECS_DIR, "1200@600PwaveMono.csv")
    wls, eff = np.loadtxt(filename, delimiter = ",", skiprows=1).T
    effPwave = interp1d(wls * 1e3, eff
                       , bounds_error = False
                       , fill_value   = "extrapolate"
                       )

    filename = os.path.join(SPECS_DIR, "1200@600SwaveMono.csv")
    wls, eff = np.loadtxt(filename, delimiter = ",", skiprows=1).T
    effSwave = interp1d(wls * 1e3, eff
                       , bounds_error = False
                       , fill_value   = "extrapolate"
                       )

    def summed_eff(x):
        return effPwave(x) + effSwave(x)

    return effPwave, effSwave, summed_eff


def load_power_ratio(filename : str = _default_power_ratio_file):
    data = np.load(filename)
    wls  = data[0 , 1:]
    pols = data[1:, 0 ]
    data = data[1:, 1:]

    x, y = np.meshgrid(wls, pols)
    return interp2d(x, y, data, kind="cubic")


def load_LiF_absorption():
    filename = os.path.join(DATABASE_DIR, "LiF_absorption.dat")
    Es, mus  = np.loadtxt(filename, skiprows=3).T
    return interp1d(Es, mus
                   , kind         = "cubic"
                   , bounds_error = False
                   , fill_value   = "extrapolate"
                   )


def spf_position(ex_wl):
    if 390 <= ex_wl < 440: return 1 # 450 nm
    if 440 <= ex_wl < 490: return 2 # 500 nm
    if 490 <= ex_wl < 540: return 3 # 550 nm
    if 540 <= ex_wl < 590: return 4 # 600 nm
    if 590 <= ex_wl < 640: return 5 # 650 nm
    return 6


def lpf_position(ex_wl):
    if 350 <= ex_wl < 400: return 1 # 400 nm
    if 400 <= ex_wl < 450: return 2 # 450 nm
    if 450 <= ex_wl < 500: return 3 # 500 nm
    if 500 <= ex_wl < 550: return 4 # 550 nm
    if 550 <= ex_wl      : return 5 # 600 nm
    return 6


def flipper_position_exc(ex_wl):
    # 355 nm lpf
    # >= 450 nm and not 360 nm because lambda/2
    # is 225 nm which will not penetrate in air
    return "up" if ex_wl >= 450 else "down"


def flipper_position_em (ex_wl):
    # 280 nm lpf
    return "up" if ex_wl >= 250 else "down"


def grating_from_blaze(blaze):
    blaze = str(blaze).strip()
    if   blaze == "300nm": return 1
    elif blaze == "500nm": return 2
    else                 : raise  ValueError


def crystal_type(crystal_no : int) -> Union[None, str]:
    if crystal_no <=   0: return None
    if crystal_no <=  40: return "Sp"
    if crystal_no <=  80: return "Qz"
    if crystal_no <= 120: return "MgF"
    if crystal_no <= 160: return "LiF"
    if crystal_no <= 200: return "BaF"
    if crystal_no <= 240: return "LiT"
    if crystal_no <= 280: return "LiN"
    if crystal_no <= 320: return "CaF"
    if crystal_no <= 360: return "MgO" # 343 onwards don't exist?
    if crystal_no <  400: return "ZnO"
    if crystal_no <  420: return "CaF"
    if crystal_no <  440: return "BaF"
    if crystal_no <  460: return "Qz"
    if crystal_no <  480: return "LiF"
    if crystal_no <  500: return "MgF"
    return None

from collections import namedtuple

import numpy  as np

from scipy.stats    import chi2      as chi_square
from scipy.optimize import curve_fit


from CCcore import in_range


FitResult = namedtuple("FitResult"
                      , "fn values errors chi2 pvalue cov"
                      )


def bin_centers(x):
    return x[:-1] + np.diff(x) / 2


def poisson_sigma(x, default=3):
    """
    Get the uncertainty of x (assuming it is poisson-distributed).
    Set *default* when x is 0 to avoid null uncertainties.
    """
    return np.where(x > 0, np.sqrt(x), default)


def get_errors(cov):
    """
    Find errors from covariance matrix

    Parameters
    ----------
    cov : np.ndarray
        Covariance matrix of the fit parameters.

    Returns
    -------
    err : 1-dim np.ndarray
        Errors asociated to the fit parameters.
    """
    return np.sqrt(np.diag(cov))


def get_chi2_and_pvalue(ydata, yfit, ndf, sigma=None):
    """
    Gets reduced chi2 and p-value

    Parameters
    ----------
    ydata : np.ndarray
        Data points.
    yfit : np.ndarray
        Fit values corresponding to ydata array.
    sigma : np.ndarray
        Data errors. If sigma is not given, it takes the poisson case:
            sigma = sqrt(ydata)
    ndf : int
        Number of degrees of freedom
        (number of data points - number of parameters).

    Returns
    -------
    chi2 : float
        Reduced chi2 computed as:
            chi2 = [sum(ydata - yfit)**2 / sigma**2] / ndf
    pvalue : float
        Fit p-value.
    """

    if sigma is None:
        sigma = poisson_sigma(ydata)

    chi2   = np.sum(((ydata - yfit) / sigma)**2)
    pvalue = chi_square.sf(chi2, ndf)

    return chi2 / ndf, pvalue


# ###########################################################
# Functions
def gauss(x, amp, mu, sigma):
    if sigma <= 0:
        return np.inf
    return amp/(2*np.pi)**.5/sigma * np.exp(-0.5*(x-mu)**2./sigma**2.)


def double_gauss(x, amp1, mu1, sigma1, amp2, mu2, sigma2):
    return (gauss(x, amp1, mu1, sigma1) +
            gauss(x, amp2, mu2, sigma2) )


def triple_gauss(x, amp1, mu1, sigma1, amp2, mu2, sigma2, amp3, mu3, sigma3):
    return (gauss(x, amp1, mu1, sigma1) +
            gauss(x, amp2, mu2, sigma2) +
            gauss(x, amp3, mu3, sigma3) )


def polynom(x, *coeffs):
    # coeffs = (c0, c1, c2, ..., cn)
    return np.polynomial.polynomial.polyval(x, coeffs)


def gauss_poly(x, *args):
    gauss1 = args[0:3]
    poly   = args[3: ]
    return gauss(x, *gauss1) + polynom(x, *poly)


def double_gauss_poly(x, *args):
    gauss1 = args[0:3]
    gauss2 = args[3:6]
    poly   = args[6: ]
    return double_gauss(x, *gauss1, *gauss2) + polynom(x, *poly)


def triple_gauss_poly(x, *args):
    gauss1 = args[0:3]
    gauss2 = args[3:6]
    gauss3 = args[6:9]
    poly   = args[9: ]
    return triple_gauss(x, *gauss1, *gauss2, *gauss3) + polynom(x, *poly)


def expo(x, scale, slope):
    return scale * np.exp(x / slope)


def expo_poly(x, scale, mean, *args):
    return expo(x, scale, mean) + polynom(x, *args)


def power(x, const, power):
    return const * np.power(x, power)


def cos(x, a, b, c, d):
    return a * np.cos(b*(x - c)) + d


# ###########################################################
# Seed
def gauss_seed(x, y, xmin=-np.inf, xmax=np.inf):
    s   = in_range(x, xmin, xmax)
    x   = x[s]
    y   = y[s]
    sig = np.std(x) / 2
    amp = np.max(y) * np.sqrt(2*np.pi) * sig
    mu  = np.average(x, weights=y)
    return amp, mu, sig


def doublegauss_seed(x, y, xmin=-np.inf, xmax=np.inf):
    amp, mu, sig = gauss_seed(x, y, xmin, xmax)

    mu1  = np.mean([mu, max(x.min(), xmin)])
    mu2  = np.mean([mu, min(x.max(), xmax)])
    return amp, mu1, sig, amp/2, mu2, sig


# ###########################################################
# Tools
def fit(func, x, y, seed=(), fit_range=None, **kwargs):
    """
    Fit x, y data to a generic relation of already defined
    python functions.

    Parameters
    ----------
    func : function
        A callable object with signature (x, par0, par1, ...) where x is
        the value (or array of values) at which the function is evaluated
        and par represent the coefficients of the function.
    x, y : iterables
        Data sets to be fitted.
    seed : sequence
        Initial estimation of the fit parameters. Either all or none of them
        must be given.
    fit_range : tuple
        Range of x in which the fit is performed.
    Notes
    -----
    - Functions must be vectorized.

    Returns
    -------
    fitted_fun : extended function (contains values and errors)
        Fitted function.

    Examples
    --------
    >>> import numpy as np
    >>> import invisible_cities.core.fit_functions as fit
    >>> x = np.linspace(-5, 5, 100)
    >>> y = np.exp(-(x-1.)**2)
    >>> f = fit.fit(fit.gauss, x, y, (1., 2., 3))
    >>> print(f.values)
    [ 1.77245385  1.          0.70710678]
    """
    if fit_range is not None:
        sel  = in_range(x, *fit_range)
        x, y = x[sel], y[sel]
        if "sigma" in kwargs:
            kwargs["sigma"] = kwargs["sigma"][sel]

    sigma_r = kwargs.get("sigma", np.ones_like(y))
    if np.any(sigma_r <= 0):
        raise ValueError("Zero or negative value found in argument sigma. "
                         "Errors must be greater than 0.")

    kwargs['absolute_sigma'] = "sigma" in kwargs

    vals, cov = curve_fit(func, x, y, seed, **kwargs)

    fitf       = lambda x: func(x, *vals)
    fitx       = fitf(x)
    errors     = get_errors(cov)
    ndof       = len(y) - len(vals)
    chi2, pval = get_chi2_and_pvalue(y, fitx, ndof, sigma_r)


    return FitResult(fitf, vals, errors, chi2, pval, cov)

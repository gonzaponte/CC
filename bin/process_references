#!/usr/bin/env python

import os
import sys
import glob
import argparse

import pandas as pd

from CCcore import average_baselines
from CCcore import filter_df
from CCcore import check_size
from CCdb   import _default_reference_file
from CCio   import load_processed
from CCio   import df_writer


def main(input_folder, output_file, power_cut):
    Sph_columns_drop = "state datetime crystal".split()
    ZnO_columns_drop = "state datetime crystal".split()
    Qtz_columns_drop = "state datetime crystal".split()
    bkg_columns_keep = "timestamp grating_spec exposure counts".split()
    amb_columns_keep = "timestamp grating_spec exposure counts".split()
    bsl_columns_keep = "timestamp grating_spec exposure counts".split()

    print(input_folder)
    print("---> Loading data")
    baselines = os.path.join(input_folder, "*crystal_-6*signal*")
    baselines = glob.glob(baselines)
    bsl = load_processed(baselines)
    bsl = average_baselines(bsl).filter(items=bsl_columns_keep)

    data = load_processed(input_folder, bsl.counts.values, cut=power_cut)
    date = pd.DataFrame(dict(basename=[os.path.basename(input_folder)]))
    Sph  = filter_df(data, crystal=-1).drop(columns=Sph_columns_drop)
    ZnO  = filter_df(data, crystal=-2).drop(columns=ZnO_columns_drop)
    Qtz  = filter_df(data, crystal=-3).drop(columns=Qtz_columns_drop)
    bkg  = filter_df(data, crystal=-4).filter(items=bkg_columns_keep)
    amb  = filter_df(data, crystal=-5).filter(items=amb_columns_keep)

    print("---> Writing data")
    df_writer(Sph , output_file, None, "Sp"        , mode="a")
    df_writer(ZnO , output_file, None, "ZnO"       , mode="a")
    df_writer(Qtz , output_file, None, "Qz"        , mode="a")
    df_writer(bkg , output_file, None, "Background", mode="a")
    df_writer(amb , output_file, None, "Ambient"   , mode="a")
    df_writer(bsl , output_file, None, "Baseline"  , mode="a")
    df_writer(date, output_file, None, "Log"       , mode="a")

    folder_size = check_size(input_folder)
    file_size   = check_size(output_file)

    print( "Done!")
    print(f"Input folder size: {folder_size:>5}")
    print(f"Output  file size: {  file_size:>5}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input-folder", type=os.path.abspath, nargs="+"                      )
    parser.add_argument("-o", "--output-file" , type=os.path.abspath, default=_default_reference_file)
    parser.add_argument("-p", "--power-cut"   , type=float          , default= 0.9                   )

    args = parser.parse_args()
    print("Input args:", args)

    if args.output_file is None:
        print("Warning: no output file provided. Using folder name as output file name.")

    output_file = args.output_file
    for input_folder in args.input_folder:
        if args.output_file is None:
            output_file = f"{input_folder}.h5"

        try:
            processed_folders = pd.read_hdf(output_file, "/Log").basename.values.tolist()
        except (FileNotFoundError, KeyError):
            processed_folders = []

        if os.path.basename(input_folder) in processed_folders:
            print(f"{input_folder} already logged, skipping.")
            continue

        os.makedirs(os.path.dirname(output_file), exist_ok=True)

        main(input_folder, output_file, args.power_cut)
